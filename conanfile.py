from conans import ConanFile, CMake, tools
import os
import shutil

class LuaConan(ConanFile):
    name = "lua"
    version = "5.4.1"
    license = "MIT"  
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-lua/"
    description = "Lua is a powerful, efficient, lightweight, embeddable scripting language."
    topics = ("lua", "scripting")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports = "CMakeLists.txt"

    def source(self):
        targz_name = "lua-{}.tar.gz".format(self.version)
        tools.download("http://www.lua.org/ftp/lua-{}.tar.gz".format(self.version), targz_name)
        tools.untargz(targz_name)
        shutil.move("lua-{}".format(self.version), "lua")
        os.unlink(targz_name)
        shutil.copyfile("CMakeLists.txt", "lua/CMakeLists.txt")
#         tools.replace_in_file("hello/CMakeLists.txt", "PROJECT(HelloWorld)",
#                               '''PROJECT(HelloWorld)
# include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
# conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="lua")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="lua/src")
        self.copy("*.hpp", dst="include", src="lua/src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["lua"]

